class videoWrapper extends HTMLElement {
  constructor() {
    super()
    this.media = 'only screen and (min-width: 750px)';
    this.dataSelectors = {
      desktop: 'data-desktop-src',
      mobile: 'data-mobile-src',
      all: 'data-src'
    };
  }
  connectedCallback() {
    this.checkVideos()
  }

  checkVideos() {
    setTimeout(() => {
      if (window.matchMedia(this.media).matches) {
        this.replaceSrc(this.dataSelectors.desktop)
      } else {
        this.replaceSrc(this.dataSelectors.mobile)
      }
    }, 250)
  }

  replaceSrc(data) {
    let srcItem;
    let videoItem;
    if (!this.querySelector(`[${data}]`)) {
      data = this.dataSelectors.all
    }
    srcItem = this.querySelector(`[${data}]`)
    srcItem.src = srcItem.getAttribute(data)
    srcItem.removeAttribute(data)
    videoItem = this.querySelector('video')
    videoItem.load()
    videoItem.play();
  }

}
customElements.define('video-wrapper', videoWrapper)
