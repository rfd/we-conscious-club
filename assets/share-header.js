class shareHeader extends HTMLElement {
  constructor() {
    super();
    this.elements = {
      shareButton: this.querySelector('.toggleShare'),
      shareCopyLink: this.querySelector('.copyLink'),
      successMessage: this.querySelector('[id^="ShareMessage"]'),
    }
    this.addEventListener('mouseover', this.openShare.bind(this));
    this.addEventListener('mouseout', this.onMouseOut.bind(this));
    this.elements.shareCopyLink.addEventListener('click', this.copyToClipboard.bind(this));
  }

  onMouseOut(event) {
    let e = event.toElement || event.relatedTarget;
    if (e.parentNode === this ||
      e === this) {
      return;
    }
    this.closeShare()
  };

  openShare() {
    this.classList.add('show-items')
  }
  closeShare() {
    this.classList.remove('show-items')
  }
  copyToClipboard() {
    navigator.clipboard.writeText(this.elements.shareCopyLink.dataset.link).then(() => {
      this.elements.successMessage.classList.remove('hidden');
      this.elements.successMessage.textContent = window.accessibilityStrings.shareSuccess;
      setTimeout(() => this.elements.successMessage.classList.add('hidden'), 3000);
    });
  }
}

customElements.define('share-header', shareHeader);
